

def get_function(o, func_name: str):
    try:
        return getattr(o, func_name)
    except AttributeError:
        return
