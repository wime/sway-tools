import typer

from sway_tools.system.theme import theme_utils


app = typer.Typer()


@app.command()
def main(config_path: str = "~/.config/sway-tools/theme.toml"):
    theme_utils.toggle_theme_from_config(config_path=config_path)


if __name__ == "__main__":
    app()
