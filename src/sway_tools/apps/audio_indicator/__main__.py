import subprocess
import toml
import typer
from sway_tools import shell
from sway_tools.system import system_status


app = typer.Typer()


ICONS = {
    "raise": "audio-volume-high",
    "lower": "audio-volume-medium",
    "muted": "audio-volume-muted",
}


@app.command()
def main(
    mode: str = "raise",
    config_path: str = "~/.config/sway-tools/indicators.toml"
) -> None:
    """
    Show indicator for current volume.

    Parameters
        mode {str} -- 'raise', 'lower' or 'muted', defines wich icon to use.
        config_path {str} -- path to config file.
    """
    config = toml.load(shell.read_path(config_path))
    try:
        icons = config["icons"]
    except KeyError:
        icons = ICONS
    audio = system_status.get_audio()
    if audio.state == "muted":
        mode = "muted"
    while mode not in icons.keys():
        mode = input("`mode`: ")
        if mode.lower() in ["q", "quit"]:
            return
    icon = icons[mode]
    cmd = f"notify-send -h string:x-canonical-private-synchronous:sys-notify\
    -u low '{audio.quantity.value}{audio.quantity.unit}'\
    -a 'volume' -i '{icon}'"
    subprocess.run(cmd, shell=True)


if __name__ == "__main__":
    main()
