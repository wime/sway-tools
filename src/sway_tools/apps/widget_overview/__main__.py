import typer
import threading
import time
from sway_tools.widgets import widgets


app = typer.Typer()


def status(config_path, mode):
    widgets.widget(name="status", config_path=config_path, mode=mode)


def calendar(config_path, mode):
    widgets.widget(name="calendar", config_path=config_path, mode=mode)


def workspaces(config_path, mode):
    widgets.widget(name="workspaces", config_path=config_path, mode=mode)


@app.command()
def main(
    mode: str = "widget",
    config_path: str = "~/.config/sway-tools/widgets.toml"
):
    threading.Thread(target=status, args=(config_path, mode)).start()
    time.sleep(0.1)
    threading.Thread(target=calendar, args=(config_path, mode)).start()
    time.sleep(1)
    threading.Thread(target=workspaces, args=(config_path, mode)).start()


if __name__ == "__main__":
    app()
