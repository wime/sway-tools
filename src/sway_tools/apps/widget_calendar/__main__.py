import typer
from sway_tools.widgets import widgets


app = typer.Typer()


@app.command()
def main(
    mode: str = "widget",
    config_path: str = "~/.config/sway-tools/widgets.toml"
):
    widgets.widget(name="calendar", config_path=config_path, mode=mode)


if __name__ == "__main__":
    app()
