import subprocess

from sway_tools import shell


def check_app_is_running(app: str) -> bool:
    """
    Check if application is running.

    Parameters
        app {str} -- Application name.
    Returns
        {bool} -- True if running, False if not.
    """
    pid = shell.get_shell_output(f"pidof {app}").replace("\n", "")
    return bool(pid)


def run_app(
    app: str,
    force_restart: bool = False,
    notify: bool = False,
    notification_icon: str = ""
) -> None:
    """Start application"""
    if force_restart:
        subprocess.run(["killall", app])
    subprocess.run(f"swaymsg exec '{app}'", shell=True)
    if notify:
        subprocess.run(
            ["notify-send", f"{app} activated.", "-i", notification_icon]
        )
