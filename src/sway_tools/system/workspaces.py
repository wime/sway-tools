from dataclasses import dataclass, field, InitVar
import json
from typing import Iterable, Optional, Dict
import toml
from sway_tools import shell


@dataclass
class App:
    name: str
    pid: int
    focused: bool = False
    content: str = ""
    icon: str = field(init=False)

    def __post_init__(self):
        name = self.name.replace(" ", "").lower()
        config = toml.load(
            shell.read_path("~/.config/sway-tools/workspaces.toml")
        )
        self.icon = config["gui_icons"].get(
            name,
            config["gui_icons"]["default"]
        )


@dataclass
class Workspace:
    name: str
    apps: Iterable[App]
    icon: Optional[str] = None
    focused: bool = field(init=False)

    def __set_focused(self):
        try:
            self.focused = max([app.focused for app in self.apps])
        except ValueError:
            self.focused = False

    def __post_init__(self):
        self.__set_focused()

    def __str__(self, spaces: int = 1):
        apps = (" " * spaces).join([app.icon for app in self.apps])
        focused = {True: "*", False: ""}
        ws = f"{focused[self.focused]}{self.icon}{self.name}"
        return f"{ws:>5}:{' '*(spaces+1)}{apps}"


def check_node(node, apps):
    try:
        app_id = node["app_id"]
        if not app_id:
            try:
                app_id = node["window_properties"]["class"]
            except KeyError:
                pass
        apps.append(
            App(
                name=app_id,
                pid=node["pid"],
                focused=node["focused"],
                content=node["name"]
            )
        )

    except KeyError:
        iterate_nodes(node, apps)


def iterate_nodes(node, apps):
    for subnode in node["nodes"]:
        check_node(subnode, apps)
    for floating_node in node["floating_nodes"]:
        check_node(floating_node, apps)


def get_workspaces():
    result = json.loads(shell.get_shell_output("swaymsg -t get_tree"))
    workspaces = []
    for screen in result["nodes"]:
        for ws in screen["nodes"]:
            apps = []
            iterate_nodes(ws, apps)
            name = ws["name"]
            if name == "__i3_scratch":
                name = "0"
            workspaces.append(Workspace(name=name, apps=apps, icon=" "))
    return workspaces


def workspace_overview(
    workspaces: Optional[Iterable[Workspace]] = None,
    v_spaces: int = 0,
    h_spaces: int = 1
):
    if not workspaces:
        workspaces = get_workspaces()
    ws_strings = [ws.__str__(spaces=h_spaces) for ws in workspaces]
    return ("\n"*(v_spaces+1)).join(ws_strings)


if __name__ == "__main__":
    workspace_overview()
