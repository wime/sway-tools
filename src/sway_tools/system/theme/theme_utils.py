import subprocess
from typing import Iterable, Dict
import toml

from sway_tools import shell, func_utils
from sway_tools.system import state as st_state
from sway_tools.system.theme import modules as theme_modules
from sway_tools.system.theme import template


def _get_gtk4_theme() -> str:
    """Find current color scheme preferance"""
    theme = shell.get_shell_output(
        "gsettings get org.gnome.desktop.interface color-scheme"
    )
    return theme.split("\n")[0].replace("'", "").split("-")[1]


def _theme_to_state(theme):
    value = {"light": 1, "dark": 0}[theme]
    return st_state.State(
        state=theme,
        quantity=st_state.Quantity(value=value, unit="state"),
    )


def _set_module_theme(module: str, theme: template.Theme, theme_names: Dict):
    module_theme_names = theme_names.get(module, None)
    if module_theme_names:
        module_theme = template.Theme(
            state=theme.state,
            light_theme=theme_names[module]["light"],
            dark_theme=theme_names[module]["dark"],
        )
    else:
        module_theme = theme
    func_utils.get_function(theme_modules, module)(theme=module_theme)


def set_theme(
    theme: template.Theme,
    included_modules: Iterable[str],
    theme_names: Dict = {},
):
    for module in included_modules:
        try:
            _set_module_theme(module=module, theme=theme, theme_names=theme_names)
        except Exception:
            pass
    subprocess.run(["sway", "reload"], capture_output=True)


def set_theme_from_config(
    theme: template.Theme,
    config_path: str = "~/.config/sway-tools/theme.toml",
) -> None:
    config = toml.load(shell.read_path(config_path))
    set_theme(
        theme=theme,
        included_modules=config["included_modules"],
        theme_names=config["theme_names"]
    )


def toggle_theme_from_config(
    config_path: str = "~/.config/sway-tools/theme.toml",
) -> None:
    theme = get_theme_from_toggle()
    set_theme_from_config(theme=theme, config_path=config_path)


def get_theme():
    current_theme = _get_gtk4_theme()
    return _theme_to_state(current_theme)


def get_theme_from_toggle():
    themes = ["light", "dark"]
    theme = {themes[0]: themes[1], themes[1]: themes[0]}[get_theme().state]
    return _theme_to_state(theme)
