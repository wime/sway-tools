from typing import Literal
from dataclasses import dataclass


THEMES = Literal["light", "dark"]


@dataclass
class Theme:
    state: THEMES
    light_theme: str = "light"
    dark_theme: str = "dark"

    def get_theme_name(self):
        return {"light": self.light_theme, "dark": self.dark_theme}[self.state]


