import os
import shutil
import configparser
import subprocess
from sway_tools import shell
from sway_tools.system.theme import template


def fuzzel(theme: template.Theme) -> None:
    """Set fuzzel theme."""
    theme_path = f"{os.environ['HOME']}/.config/fuzzel/themes/{theme.state}.ini"
    config_path = f"{os.environ['HOME']}/.config/fuzzel/fuzzel.ini"
    colors = configparser.ConfigParser()
    colors.read(theme_path)
    config = configparser.ConfigParser()
    config.read(config_path)
    try:
        config["colors"] = colors["colors"]
    except TypeError:
        raise TypeError(f"No `colors` section in {theme_path}")
    with open(config_path, "w") as f:
        config.write(f)


def gtk4(theme: template.Theme) -> None:
    subprocess.run(
        f"gsettings set org.gnome.desktop.interface color-scheme prefer-{theme.state}",
        shell=True
    )


def gtk3(theme: template.Theme) -> None:
    theme_name = theme.get_theme_name()
    subprocess.run(
        f"gsettings set org.gnome.desktop.interface gtk-theme {theme_name}",
        shell=True
    )


def kitty(theme: template.Theme) -> None:
    theme_name = theme.get_theme_name()
    subprocess.run(
        f"kitty +kitten themes --reload-in=all {theme_name}",
        shell=True
    )


def sway(theme: template.Theme) -> None:
    """Set theme of sway"""
    source = f"~/.config/sway/themes/theme_{theme.state}"
    destination = "~/.config/sway/config.d/theme"
    shutil.copy(shell.read_path(source), shell.read_path(destination))


def wallpaper(theme: template.Theme) -> None:
    source = f"~/.config/wallpaper/{theme.state}.jpg"
    destination = "~/.config/wallpaper/wallpaper.jpg"
    shutil.copy(shell.read_path(source), shell.read_path(destination))


def mako(theme: template.Theme) -> None:
    subprocess.run(f"makoctl mode -s {theme.state}", shell=True, capture_output=True)


def waybar(theme: template.Theme) -> None:
    source = f"~/.config/waybar/themes/{theme.state}.css"
    destination = "~/.config/waybar/theme.css"
    shutil.copy(shell.read_path(source), shell.read_path(destination))


