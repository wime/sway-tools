from dataclasses import dataclass
from numbers import Number
from typing import Literal

from sway_tools import shell


UNIT = Literal["state", "%"]


@dataclass
class Quantity:
    value: Number
    unit: UNIT


@dataclass
class State:
    state: str
    quantity: Quantity
    text: str = ""


def get_audio():
    result = shell.get_shell_output("pactl list sinks")
    for item in result.split("\n"):
        try:
            key, value = item.strip("\t").replace("\n", "").split(": ", 1)
        except ValueError:
            pass
        else:
            if key == "Volume":
                percent = float(value.split("/")[1][1:-2].replace(",", "."))
                percent = int(round(percent))
            elif key == "Mute":
                state = {"yes": "muted", "no": "volume"}[value]
            elif key == "Description":
                description = value
    if not state:
        percent = 0
    return State(
        state=state,
        quantity=Quantity(value=percent, unit="%"),
        text=description
    )


def get_battery():
    result = shell.get_shell_output("upower -i /org/freedesktop/UPower/devices/battery_BAT0")
    battery_dict = {}
    for values in result.split("\n"):
        dict_input = values.replace(" ", "").replace("\n", "").split(":")
        if len(dict_input) == 2:
            battery_dict[dict_input[0]] = dict_input[1]
    state = battery_dict["state"]
    percent = int(round(float(battery_dict["percentage"][:-1].replace(",", "."))))
    return State(
        state=state,
        quantity=Quantity(value=percent, unit="%"),
    )


def get_wifi():
    """Get wifi connection."""
    connection = shell.get_shell_output("nmcli con show --active")
    if connection == "\n":
        return State(
            state="diconnected",
            quantity=Quantity(value=0, unit="state"),
            text="NOT CONNECTED"
        )
    return State(
        state="wifi",
        quantity=Quantity(value=1, unit="state"),
        text=connection.split("\n")[1].split(" ")[0]
    )


def get_brightness():
    value = int(round(float(shell.get_shell_output("light"))))
    return State(
        state="ON",
        quantity=Quantity(value=value, unit="%"),
    )


def get_night_light():
    state = "false"
    return State(
        state={"true": "ON", "false": "OFF"}[state],
        quantity=Quantity(value=1, unit="state"),
    )


def get_caffeine():
    state = "false"
    return State(
        state={"true": "ON", "false": "OFF"}[state],
        quantity=Quantity(value=1, unit="state"),
    )

