import subprocess
import json
import os


def read_path(path: str) -> str:
    """
    Translate path to Python readable string.

    Parameters
        path {str} -- Path to translate.
    Returns
        {str} -- Translated path.
    """
    return path.replace("~", os.environ["HOME"])


def get_shell_output(cmd: str) -> str:
    """
    Get output of shell command.

    Parameters
        cmd {str} -- Shell command to execute.
    Returns
        {str} Output from shell command.
    """
    result = subprocess.run(cmd, shell=True, capture_output=True)
    return result.stdout.decode("utf-8")


def get_open_notifications():
    notifs = get_shell_output("makoctl list")
    notifs = json.loads(notifs)
    apps = [notif["app-name"]["data"] for notif in notifs["data"][0]]
    return apps


if __name__ == "__main__":
    get_open_notifications()