from typing import Iterable, Dict
import json
import os
import subprocess
import datetime as dt
from numbers import Number

from sway_tools.system import state as st_state
from sway_tools import shell


def _get_percent_bar(value: Number) -> str:
    """
    Create percent bar.

    Parameters
        value {Number} -- Percent.
    Returns
     {str} -- Percent bar.
    """
    circle_open = ""
    circle_filled = ""
    percent_rounded = round(value, -1)
    filled = int(percent_rounded * 1e-1)
    open = 10 - filled
    return filled * (circle_filled + " ") + open * (circle_open + " ")


def _get_state_icon(state: str, icons: Iterable, states: Iterable) -> str:
    if len(icons) != len(states):
        raise ValueError("states and icons need to be the same length.")
    state_icons = {state: state_icon for state, state_icon in zip(states, icons)}
    return state_icons[state]


def _get_percent_icons(percent, icons):
    return icons[0]


def _get_icon(
    icon_mode,
    icons,
    states,
    system_state,
    icon_on_top: bool = False
):
    if icon_mode == "icon":
        icon = icons[0]
    elif icon_mode == "text":
        icon = system_state.state
    elif icon_mode == "state":
        icon = _get_state_icon(system_state.state, icons, states)
    elif icon_mode == "percent":
        icon = _get_percent_icons(system_state.quantity.value, icons)
    else:
        icon = icons[0]
    if icon_on_top:
        icon = icon + "\n"
    return icon


def text_module(
    text: str,
    icon: str = "",
    icon_on_top: bool = True
) -> str:
    """
    Module consisting of text and an optionl icon.

    Parameters
        text {str} -- The text to display.
        icon {str} -- An optional icon. EMpty text string means no icon.
        icon_on_top {bool} -- If the icon is above the text.
    Returns
        {str} -- The resulting module to display.
    """
    if len(icon) > 0 and icon_on_top:
        icon = icon + "\n"
    return f"{icon}{text}"


def state_module(
    system_state: st_state.State,
    icon_mode: str = "icon",
    icons: Iterable = [],
    states: Iterable = ["ON", "OFF"],
    show_state: bool = True,
    show_text: bool = False,
    icon_on_top: bool = False
):
    icon = _get_icon(icon_mode, icons, states, system_state, icon_on_top)
    if show_state:
        state_text = "  " + system_state.state
    else:
        state_text = ""
    if show_text:
        text = "  " + system_state.text
    else:
        text = ""
    return f"{icon}{state_text}{text}"


def percent_module(
    system_state: st_state.State,
    icon_mode: str = "text",
    icons: Iterable = [],
    states: Iterable = ["ON", "OFF"],
    show_percent_bar: bool = True,
    show_percent: bool = True,
    icon_on_top: bool = False
) -> str:
    icon = _get_icon(icon_mode, icons, states, system_state, icon_on_top)
    if show_percent_bar:
        percent_bar = "  " + _get_percent_bar(system_state.quantity.value)
    else:
        percent_bar = ""
    if show_percent:
        percent_text = f" {int(system_state.quantity.value):>3}{system_state.quantity.unit}"
    else:
        percent_text = ""
    return f"{icon:<2}{percent_bar}{percent_text}"


def percent_module_from_config(
    system_state: st_state.State,
    config: Dict
):
    return percent_module(
        system_state=system_state,
        icon_mode=config.get("icon_mode", "icon"),
        icons=config.get("icons", [" "]),
        states=config.get("states", ["ON", "OFF"]),
        show_percent_bar=config.get("show_percent_bar", True),
        show_percent=config.get("show_percent", True),
        icon_on_top=config.get("icon_on_top", False),
    )


def state_module_from_config(
    system_state: st_state.State,
    config: Dict
):
    return state_module(
        system_state=system_state,
        icon_mode=config.get("icon_mode", "icon"),
        icons=config.get("icons", [" "]),
        states=config.get("states", ["ON", "OFF"]),
        show_state=config.get("show_state", True),
        show_text=config.get("show_text", False),
        icon_on_top=config.get("icon_on_top", False),
    )


def custom_module(config: Dict, name: str = "Widget"):
    if config.get("refresh", 0) > 0:
        path = f"~/.temp/widget_{name}.json"
        try:
            mod_ts = dt.datetime.fromtimestamp(os.path.getmtime(shell.read_path(path)))
        except FileNotFoundError:
            module = shell.get_shell_output(cmd=shell.read_path(config["script"]))
            module = json.loads(module)
            subprocess.run(["touch", shell.read_path(path)])
            with open(shell.read_path(path), "w") as f:
                json.dump(module, f)
        else:
            now = dt.datetime.now()
            if (now - mod_ts).total_seconds() > config["refresh"]*0.7:
                module = shell.get_shell_output(cmd=shell.read_path(config["script"]))
                module = json.loads(module)
                subprocess.run(["touch", shell.read_path(path)])
                with open(shell.read_path(path), "w") as f:
                    json.dump(module, f)
            else:
                with open(shell.read_path(path), "r") as f:
                    module = json.load(f)
    else:
        module = shell.get_shell_output(cmd=shell.read_path(config["script"]))
        module = json.loads(module)

    if config.get("module_type", "text") == "text":
        return text_module(
            text=module["text"],
            icon=config.get("icon", ""),
            icon_on_top=config.get("icon_on_top", False)
        )
