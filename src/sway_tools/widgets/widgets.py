import subprocess
from typing import Iterable, Dict
import toml
import time
from sway_tools import shell, func_utils
from sway_tools.widgets import modules as widget_modules
from sway_tools.widgets import templates as widget_templates


def show(modules: Iterable, mode: str = "widget", app_name: str = "st-widgets"):
    text = "\n".join(modules)
    if mode == "widget":
        subprocess.run(
            [
                "notify-send",
                text,
                "-a",
                app_name,
                "-h",
                "string:x-canonical-private-synchronous:sys-notify"
            ]
        )
    else:
        print(text)


def _names_to_modules(module_names: Iterable, config: Dict):
    modules = []
    for module_name in module_names:
        if module_name in config["custom_modules"]:
            module_config = config["custom_modules"][module_name]
            modules.append(widget_templates.custom_module(config=module_config, name=module_name))
        if module_name in config["built_in_modules"]:
            module_func = func_utils.get_function(o=widget_modules, func_name=module_name.split("-")[0])
            module_config = config["built_in_modules"][module_name]
            modules.append(module_func(config=module_config))
    return [module for module in modules if module is not None]


def widget(
    name: str = "widget",
    config_path: str = "~/.config/sway-tools/widgets.toml",
    mode: str = "widget",
):
    keep_going = True
    while keep_going:
        config = toml.load(shell.read_path(config_path))
        module_names = config["widgets"][name]["modules"]
        modules = _names_to_modules(module_names, config)
        show(
            modules,
            app_name=config["widgets"][name].get("app_name", f"widgets_{name}"),
            mode=mode
        )
        time.sleep(config["widgets"][name]["refresh"])
        if config["widgets"][name]["app_name"] not in shell.get_open_notifications():
            keep_going = False
            return



