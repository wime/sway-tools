from typing import Dict
from sway_tools import shell
from sway_tools.widgets import templates
from sway_tools.system import state as st_state
from sway_tools.system import workspaces as st_workspaces
from sway_tools.system.theme import theme_utils


def audio(config: Dict):
    return templates.percent_module_from_config(
        system_state=st_state.get_audio(),
        config=config
    )


def battery(config: Dict):
    return templates.percent_module_from_config(
        system_state=st_state.get_battery(),
        config=config
    )


def network(config: Dict):
    return templates.state_module_from_config(
        system_state=st_state.get_wifi(),
        config=config
    )


def brightness(config: Dict):
    return templates.percent_module_from_config(
        system_state=st_state.get_brightness(),
        config=config
    )


def theme(config: Dict):
    return templates.state_module_from_config(
        system_state=theme_utils.get_theme(),
        config=config
    )


def night_light(config: Dict):
    return templates.state_module_from_config(
        system_state=st_state.get_night_light(),
        config=config
    )


def caffeine(config: Dict):
    return templates.state_module_from_config(
        system_state=st_state.get_caffeine(),
        config=config
    )


def separator(config: Dict):
    width = config["width"]
    icon = config["icon"]
    return f"\n{icon * width}\n"


def timestamp(config: Dict):
    return templates.text_module(
        text=shell.get_shell_output(f"date +'{config['format']}'")[:-1].capitalize(),
        icon=config["icon"],
        icon_on_top=config["icon_on_top"]
    )


def calendar(config: Dict):
    return templates.text_module(
        text=shell.get_shell_output("cal -w")[:-1],
        icon=config["icon"],
        icon_on_top=config["icon_on_top"]
    )


def workspaces(config: Dict):
    return templates.text_module(
        text=st_workspaces.workspace_overview(
            v_spaces=config.get("v_spaces", 1),
            h_spaces=config.get("h_spaces", 2)
        ),
        icon=config.get("icon", ""),
        icon_on_top=config.get("icon_on_top", False)
    )
